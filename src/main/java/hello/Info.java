package hello;

import java.util.List;

public class Info {
    private List<String> accounts;

    public Info(List<String> accounts) {
        this.accounts = accounts;
    }

    public List<String> getAccounts() {
        return accounts;
    }
}
