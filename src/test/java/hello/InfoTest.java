package hello;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InfoTest {

    @Test
    public void testInfo() {
        List<String> list = new ArrayList<>(Arrays.asList("hello", "world"));
        Assert.assertThat(new Info(list).getAccounts().size(), CoreMatchers.is(2));
    }

    @Test
    public void testFail() {
        Assert.assertThat(true, CoreMatchers.is(false));
    }
}
